<?php

namespace App\Http\Livewire;

use App\Models\Post;
use Livewire\Component;
use Livewire\WithFileUploads;

class PostEdit extends Component
{
    use WithFileUploads;

    public $post;
    public $title;
    public $content;
    public $photo;
    public $successMessage;
    public $tempUrl;

    protected $rules = [
        'title' => 'required',
        'content' => 'required',
        'photo' => 'nullable|sometimes|image|max:5000',
    ];

    public function mount(Post $post)
    {
        $this->post = $post;
        $this->title = $post->title;
        $this->content = $post->content;
    }

    public function updatedPhoto()
    {
        try {
            $this->tempUrl = $this->photo->temporaryUrl();
        } catch (\Exception $e) {
            $this->tempUrl = '';
        }
        $this->validate();
    }

    public function editPost()
    {
        $this->validate();

        $showPhoto = $this->photo ?? null;

        /**
         * Store As
         * - folder in public path
         * - image file name with extention
         * - kat mana simpan
         */
        // $uploaded = $this->photo->storeAs('photos', str_replace(' ', '', microtime()) . '.' . $this->photo->extension(), 'public' );
        $photo = $this->photo
            ?
            $this->photo->storeAs('photos', str_replace(' ', '', microtime()) . '.' . $this->photo->extension(), 'public' )
            : //or
            $showPhoto;

        $this->post->update([
            'title' => $this->title,
            'content' => $this->content,
            'photo' => $photo,
        ]);

        $this->successMessage = 'Post was updated successfully!';

    }
    public function render()
    {
        return view('livewire.post-edit');
    }
}
