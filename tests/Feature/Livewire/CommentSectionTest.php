<?php

namespace Tests\Feature\Livewire;

use App\Http\Livewire\CommentSection;
use App\Models\Comment;
use App\Models\Post;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Livewire\Livewire;
use Tests\TestCase;

class CommentSectionTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function main_page_contains_comment_section_livewire_component()
    {
        $post = Post::factory()->create();

        $this->get('/posts/' . $post->id)
            ->assertSeeLivewire('comment-section');
    }

    /** @test */
    public function comment_post_works_correctly()
    {
        $post = Post::factory()->create();

        Livewire::test(CommentSection::class)
            ->set('post', $post)
            ->set('comment', 'This is new comment')
            ->call('postComment')
            ->assertSee('Comment was posted!')
            ->assertSee('This is new comment');
    }

    /** @test */
    public function comment_is_required_validation_works_correctly()
    {
        $post = Post::factory()->create();

        Livewire::test(CommentSection::class)
            ->set('post', $post)
            // ->set('comment', 'This is new comment')
            ->call('postComment')
            ->assertSee('The comment field is required.');
    }

    /** @test */
    public function comment_minimum_validation_works_correctly()
    {
        $post = Post::factory()->create();

        Livewire::test(CommentSection::class)
            ->set('post', $post)
            ->set('comment', 'Thi')
            ->call('postComment')
            ->assertSee('The comment must be at least 4 characters.');
    }
}
