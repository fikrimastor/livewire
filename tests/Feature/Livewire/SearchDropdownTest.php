<?php

namespace Tests\Feature\Livewire;

use App\Http\Livewire\SearchDropdown;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Livewire\Livewire;
use Tests\TestCase;

class SearchDropdownTest extends TestCase
{
    // /** @test */
    // public function the_component_can_render()
    // {
    //     $component = Livewire::test(SearchDropdown::class);

    //     $component->assertStatus(200);
    // }

    /** @test */
    public function main_page_has_livewire_component()
    {
        $this->get('/')
            ->assertSeeLivewire('search-dropdown');
    }

    /** @test */
    public function search_dropdown_searches_correctly()
    {
        Livewire::test(SearchDropdown::class)
            ->assertDontSee('The Sopranos')
            ->set('search', 'Isabella')
            ->assertSee('The Sopranos');
    }

    /** @test */
    public function search_dropdown_not_found_song()
    {
        Livewire::test(SearchDropdown::class)
            ->set('search', 'asdasdasdasfafwqfq')
            ->assertSee('No results found for');
    }
}
