<?php

namespace App\Http\Livewire;

use App\Models\Post;
use App\Models\Comment;
use Livewire\Component;

class CommentSection extends Component
{
    public $post;
    public $comment;
    public $successMessage;

    public function mount(
        Post $post
    )
    {
        $this->post = $post;
    }

    protected $rules =  [
        'comment' => 'required|min:4',
        'post' => 'required',
    ];

    public function postComment()
    {
        $this->validate();

        \sleep(1);

        Comment::create([
            'post_id' => $this->post->id,
            'username' => fake()->name(),
            'content' => $this->comment,
        ]);

        $this->comment = '';

        $this->post = Post::find($this->post->id)->first();

        $this->successMessage = 'Comment was posted!';
    }

    public function render()
    {
        return view('livewire.comment-section');
    }
}
