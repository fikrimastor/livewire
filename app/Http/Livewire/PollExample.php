<?php

namespace App\Http\Livewire;

use App\Models\Order;
use Livewire\Component;

class PollExample extends Component
{
    public $order;

    // public function mount(Order $order)
    public function mount()
    {
        // $this->order = $order;
        $this->order = $this->totalSale();
    }

    public function totalSale()
    {
        $this->order = Order::all()->sum('price');

        return $this->order;
    }

    public function render()
    {
        return view('livewire.poll-example');
    }
}
