{{-- <x-form.field>
    <button type="submit" class="px-4 py-2 text-white bg-blue-500 rounded hover:bg-blue-600">
        {{$slot}}
    </button>
</x-form.field> --}}

<div class="mt-8 border-t border-gray-200 pt-5">
    <div class="flex justify-end">
        <span class="ml-3 inline-flex rounded-md shadow-sm">
            <button type="submit"
                class="inline-flex justify-center py-2 px-4 border border-transparent text-sm leading-5 font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700 transition duration-150 ease-in-out">
                {{$slot}}
            </button>
        </span>
    </div>
</div>
