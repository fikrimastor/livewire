<?php

namespace Tests\Feature\Livewire;

use App\Http\Livewire\TagComponent;
use App\Models\Tag;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Livewire\Livewire;
use Tests\TestCase;

class TagComponentTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function page_contains_post_edit_livewire_component()
    {
        $this->get('/')
            ->assertSeeLivewire('tag-component');
    }

    /** @test */
    public function load_existing_tags()
    {
        $tagA = Tag::create(['name' => 'one']);
        $tagB = Tag::create(['name' => 'two']);

        Livewire::test(TagComponent::class)
            ->assertSet('tags', json_encode([$tagA->name, $tagB->name]));
    }

    /** @test */
    public function add_tags_correctly()
    {
        $tagA = Tag::create(['name' => 'one']);
        $tagB = Tag::create(['name' => 'two']);

        Livewire::test(TagComponent::class)
            ->emit('tagAdded', 'three')
            ->assertEmitted('tagAddedFromBackend', 'three');

        $this->assertDatabaseHas('tags', [
            'name' => 'three'
        ]);
    }

    /** @test */
    public function delete_tags_correctly()
    {
        $tagA = Tag::create(['name' => 'one']);
        $tagB = Tag::create(['name' => 'two']);

        Livewire::test(TagComponent::class)
            ->emit('tagRemoved', 'two');

        $this->assertDatabaseMissing('tags', [
            'name' => 'two'
        ]);
    }
}
