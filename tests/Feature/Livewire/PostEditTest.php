<?php

namespace Tests\Feature\Livewire;

use App\Http\Livewire\PostEdit;
use App\Models\Post;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Livewire\Livewire;
use Tests\TestCase;

class PostEditTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function page_contains_post_edit_livewire_component()
    {
        $post = Post::factory()->create();

        // $this->get('/posts/' . $post->id . '/edit')
        $this->get(route('post.edit', $post->id))
            ->assertSeeLivewire('post-edit');
    }

    /** @test */
    public function form_edit_works_correctly()
    {
        $post = Post::factory()->create();

        Livewire::test(PostEdit::class, ['post' => $post])
            ->set('title', 'New title')
            ->set('content', 'New content')
            ->call('editPost');

        $post->refresh();
        $this->assertEquals('New title', $post->title);
        $this->assertEquals('New content', $post->content);


    }

    /** @test */
    public function form_edit_upload_photo_works_correctly()
    {
        $post = Post::factory()->create();

        Storage::fake('public');

        $file = UploadedFile::fake()->image('image.jpg');

        Livewire::test(PostEdit::class, ['post' => $post])
            ->set('title', 'New title')
            ->set('content', 'New content')
            ->set('photo', $file)
            ->call('editPost');

        $post->refresh();
        $this->assertNotNull($post->photo);

        Storage::disk('public')->assertExists($post->photo);
    }

    /** @test */
    public function form_edit_upload_photo_validation_works_correctly()
    {
        $post = Post::factory()->create();

        Storage::fake('public');

        $file = UploadedFile::fake()->create('docs.pdf');

        Livewire::test(PostEdit::class, ['post' => $post])
            ->set('title', 'New title')
            ->set('content', 'New content')
            ->set('photo', $file)
            ->call('editPost')
            ->assertHasErrors([
                'photo' => 'image'
            ]);

        $post->refresh();

        $this->assertNull($post->photo);
        Storage::disk('public')->assertMissing($post->photo);
    }
}
