<?php

namespace Tests\Feature\Livewire;

use App\Http\Livewire\PollExample;
use App\Models\Order;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Livewire\Livewire;
use Tests\TestCase;

class PollExampleTest extends TestCase
{
    // /** @test */
    // public function the_component_can_render()
    // {
    //     $component = Livewire::test(PollExample::class);

    //     $component->assertStatus(200);
    // }

    use RefreshDatabase;

    /** @test */
    public function main_page_contains_poll_section_livewire_component()
    {
        $this->get('/')
            ->assertSeeLivewire('poll-example');
    }

    /** @test */
    public function poll_order_works_correctly()
    {
        $orders[] = Order::create([
            'price' => fake()->randomDigitNotZero()
        ])->price;

        $orders[] = Order::create([
            'price' => fake()->randomDigitNotZero()
        ])->price;

        $sum = array_sum($orders);

        Livewire::test(PollExample::class)
            ->call('totalSale')
            ->set('order', $sum)
            ->assertSee('Total Sale: RM ' . $sum);

        $orders[] = Order::create([
            'price' => fake()->randomDigitNotZero()
        ])->price;

        $sum = array_sum($orders);

        Livewire::test(PollExample::class)
            ->call('totalSale')
            ->set('order', $sum)
            ->assertSee('Total Sale: RM ' . $sum);
    }
}
