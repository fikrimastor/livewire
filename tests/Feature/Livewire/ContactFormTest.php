<?php

namespace Tests\Feature\Livewire;

use Tests\TestCase;
use Livewire\Livewire;
use App\Http\Livewire\ContactForm;
use Illuminate\Support\Facades\Log;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ContactFormTest extends TestCase
{
    /** @test */
    public function main_page_contains_contact_form_livewire_component()
    {
        $this->get('/')
            ->assertSeeLivewire('contact-form');
    }

    /** @test */
    public function contact_form_save_in_logs()
    {
        Livewire::test(ContactForm::class)
            ->set('name', 'AFM')
            ->set('email', 'afm@admin.com')
            ->set('phone', '123456')
            ->set('message', 'This is my message')
            ->call('submitForm')
            ->assertSee('We received your message successfully and will get back to you shortly!');

    }

    /** @test */
    public function contact_form_field_is_required()
    {
        Livewire::test(ContactForm::class)
            // ->set('name', 'AFM')
            ->set('email', 'afm@admin.com')
            ->set('phone', '123456')
            ->set('message', 'This is my message')
            ->call('submitForm')
            ->assertHasErrors([
                'name' => 'required'
            ]);

    }

    /** @test */
    public function contact_form_field_has_minimum_character()
    {
        Livewire::test(ContactForm::class)
            // ->set('name', 'AFM')
            ->set('email', 'afm')
            ->set('phone', '123456')
            ->set('message', 'Thi')
            ->call('submitForm')
            ->assertHasErrors([
                'message' => 'min'
            ]);

    }
}
