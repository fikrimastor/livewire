<?php

use App\Models\Post;
use App\Models\User;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('example', [
        // 'users' => User::paginate(25)
        'posts' => Post::paginate(25)
    ]);
});

Route::get('/drop', function () {
    return view('dropdown');
});

// Route::get('/posts/{post}', function(Post $post) {
//     return view('posts.show', ['post' => $post]);
// })->name('post.show');

// Route::get('/posts/{post}/edit', function(Post $post) {
//     return view('posts.edit', ['post' => $post]);
// })->name('post.edit');

Route::group([
    'controller' => PostController::class
], function () {
    Route::get('/posts/{post}', 'show')->name('post.show');
    Route::get('/posts/{post}/edit', 'edit')->name('post.edit');
});
