<?php

namespace Tests\Feature\Livewire;

use Tests\TestCase;
use App\Models\User;
use Livewire\Livewire;
use App\Http\Livewire\DataTables;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DataTablesTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function main_page_has_livewire_component()
    {
        $this->get('/')
            ->assertSeeLivewire('data-tables');
    }

    // /** @test */
    // public function datatables_search_show_correct_results()
    // {
    //     Livewire::test(SearchDropdown::class)
    //         // ->assertDontSee('Fikri')
    //         ->set('search', 'Fikri')
    //         ->assertSee('fikri@admin.com');
    // }

    /** @test */
    public function datatables_active_checkbox_works_correctly()
    {

        $userA = User::create([
            'name' => 'UserA',
            'email' => 'user.a@user.com',
            'password' => bcrypt('password'),
            'active' => true
        ]);

        $userB = User::create([
            'name' => 'UserB',
            'email' => 'user.b@user.com',
            'password' => bcrypt('password'),
            'active' => false
        ]);

        Livewire::test(DataTables::class)
            ->assertSee($userA->name);
    }

    /** @test */
    public function datatables_not_active_checkbox_works_correctly()
    {

        $userA = User::create([
            'name' => 'UserA',
            'email' => 'user.a@user.com',
            'password' => bcrypt('password'),
            'active' => true
        ]);

        $userB = User::create([
            'name' => 'UserB',
            'email' => 'user.b@user.com',
            'password' => bcrypt('password'),
            'active' => false
        ]);

        Livewire::test(DataTables::class)
            ->set('active', false)
            ->assertSee($userB->name);
    }

    /** @test */
    public function datatables_search_works_correctly()
    {

        $userA = User::create([
            'name' => 'UserA',
            'email' => 'user.a@user.com',
            'password' => bcrypt('password'),
            'active' => true
        ]);

        $userB = User::create([
            'name' => 'UserB',
            'email' => 'user.b@user.com',
            'password' => bcrypt('password'),
            'active' => false
        ]);

        Livewire::test(DataTables::class)
            ->set('search', 'UserA')
            ->assertSee($userA->name);

        Livewire::test(DataTables::class)
            ->set('search', 'user.a')
            ->assertSee($userA->email);
    }
}
